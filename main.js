
var canvas = document.getElementById("canv");
var ctx = canvas.getContext("2d");
var scale = 20;
var numBoids = 30;
var numTicks = 0;
var infinity = 100000;

//-------------------------------------------------------------------------------
function Cell(col, row) {
	this.col = col;
	this.row = row;
	this.weight = infinity;
	this.next = null;
	this.target = null;
	this.obstacles = 0;
	this.isEnd = false;
	this.timestamp = 0;
	this.boids = [ ];
	this.life = 0;
}

Cell.prototype.toggleEnd = function() {
	if (this.isEnd) {
		this.isEnd = false;
		this.weight = infinity;
		this.target = null;
	} else {
		this.isEnd = true;
		this.weight = 0;
		this.obstacles = 0;
		this.next = null;
		this.target = this;
		this.life = 100;
	}
}

Cell.prototype.toggleObstacle = function() {
	if (this.obstacles > 0) {
		this.obstacles = 0;
	} else {
		this.obstacles = 1000;
		this.weight = infinity;
	}
}

Cell.prototype.addBoid = function(boid) {
	this.boids.push(boid);
}

Cell.prototype.removeBoid = function(boid) {
	var i = this.boids.indexOf(boid);
	if (i != -1) {
		this.boids.splice(i, 1);
	}
}

//-------------------------------------------------------------------------------
function Grid(w, h) {
	this.width = w;
	this.height = h;
	this.cells = [];
	for (var i = 0; i < w; ++i) {
		var row = [];
		this.cells.push(row);
		for (var j = 0; j < h; ++j) {
			var cell = new Cell(i, j);
			row.push(cell);
		}
	}
	this.iterator = this.getIterator();
}

Grid.prototype.processCell = function(c,r) {
	var cell = this.cells[c][r];
	if (cell.obstacles > 0) {
		return;
	}

	for (var i = 0; i < edges.length; ++i) {
		var neighbour = this.cells[c + edges[i].x][r + edges[i].y];
		if (neighbour.timestamp < cell.timestamp || (neighbour.timestamp == cell.timestamp && cell.weight + edges[i].w + neighbour.obstacles < neighbour.weight)) {
			neighbour.weight = cell.weight + edges[i].w + neighbour.obstacles;
			neighbour.next = cell;
			neighbour.target = cell.target;
			neighbour.timestamp = cell.timestamp;
		}
	}
}

Grid.prototype.getIterator = function() {
	var c = 1, r = 1, dir = false, self = this;
	return {
		next: function() {
			if (dir) {
				if (c < self.width - 1) {
					if (r < self.height - 1) {
						self.processCell(c, r);
						r++;
					} else {
						c++;
						r = 1;
					}
				} else {
					c = self.width - 2;
					r = self.height - 2;
					dir = !dir;
				}
			} else {
				if (c > 0) {
					if (r > 0) {
						self.processCell(c, r);
						r--;
					} else {
						c--;
						r = self.height - 2;
					}
				} else {
					c = r = 1;
					dir = !dir;
				}
			}
		}
	};
}

Grid.prototype.damageCell = function(cell, dmg) {
	cell.life -= dmg;
	if (cell.life <= 0) {
		cell.toggleEnd();
		this.revalidate();
	}
}

Grid.prototype.getCell = function(x, y) {
	if (x >= 1 && y >= 1 && x < this.width-1 && y < this.height - 1) {
		return this.cells[Math.floor(x)][Math.floor(y)];
	}
	return null;
}

Grid.prototype.revalidate = function() {
	for (var i = 0; i < this.width; ++i) {
		for (var j = 0; j < this.height; ++j) {
			if (this.cells[i][j].isEnd) {
				this.cells[i][j].weight = 0;
				this.cells[i][j].timestamp = numTicks;
			}	
		}
	}
}

Grid.prototype.draw = function() {
	for (var i = 0; i < this.width; ++i) {
		for (var j = 0; j < this.height; ++j) {
			var cell = this.cells[i][j];
			if (cell.isEnd) {
				var color = Math.min(255, Math.floor(cell.life * 255 / 100));
				ctx.fillStyle = blueColors[color];
			} else {
				var color =  Math.min(255, Math.floor(cell.weight * 255 / 30));
				ctx.fillStyle = grayColors[color];
			}
			ctx.fillRect(i * scale, j * scale, scale, scale);
			if (cell.obstacles > 0) {
				ctx.strokeStyle = '#ff0000';
				ctx.strokeRect(i * scale, j * scale, scale, scale);
			}
		}
	}
}

//-------------------------------------------------------------------------------
function Boid(grid, x, y) {
	this.grid = grid;
	this.speed = 0.08;
	this.pos = [x, y];
	this.velocity = [0, 0];
	this.cell = null;
}

Boid.prototype.separate = function() {
	for (var i = -1; i <= 1; ++i) {
		for (var j = -1; j <= 1; ++j) {
			var env = this.grid.cells[this.cell.col + i][this.cell.row + j];
			for (var k = 0; k < env.boids.length; ++k) {
				var delta = v2Diff(this.pos, env.boids[k].pos);
				if (v2Len(delta) < 0.5) {
					v2Inc(this.velocity, v2Mul(delta, 0.25));
				}
			}
		}
	}
}

Boid.prototype.damage = function() {
	if (this.cell.isEnd && this.cell.life > 0) {
		this.grid.damageCell(this.cell, 5);
	}
}

Boid.prototype.move = function() {
	var moveCell = this.grid.getCell(this.pos[0] + this.velocity[0], this.pos[1]);
	if (!moveCell || moveCell.obstacles > 0) {
		this.velocity[0] = 0;
	} else {
		this.pos[0] += this.velocity[0];
	}
	moveCell = this.grid.getCell(this.pos[0], this.pos[1] + this.velocity[1]);
	if (!moveCell || moveCell.obstacles > 0) {
		this.velocity[1] = 0;
	} else {
		this.pos[1] += this.velocity[1];
	}
	this.cell = this.grid.getCell(this.pos[0], this.pos[1]);
}

Boid.prototype.tick = function() {
	if (this.cell) {
		this.cell.removeBoid(this);

		if (this.cell.next) {
			this.velocity = v2Mul(v2Normalize( [ this.cell.next.col - this.cell.col, this.cell.next.row - this.cell.row ] ), this.speed);
		} else {
			this.velocity = [0, 0];
		}
		this.separate();
		this.damage();
	}

	this.move();

	if (this.cell) {
		this.cell.addBoid(this);
	}
}

Boid.prototype.draw = function() {
	ctx.strokeStyle = '#00FF00';
	ctx.fillStyle = '#009900';
	ctx.fillRect(this.pos[0] * scale-scale/4, this.pos[1] * scale-scale/4, scale/2, scale/2);
	ctx.strokeRect(this.pos[0] * scale-scale/4, this.pos[1] * scale-scale/4, scale/2, scale/2);
}

//
//------------------------- INIT --------------------------------------------------
//
var grid = new Grid(36, 36);
var boids = [ ];

for (var i = 0; i < numBoids; ++i) {
	boids[i] = new Boid(grid, Math.sin(i*2/numBoids*Math.PI)*13 + 18, Math.cos(i*2/numBoids*Math.PI)*13 + 18);
}

setInterval(function() {
	for (var i = 0; i < 500; ++i) {
		grid.iterator.next();
	}
	for (var i = 0; i < boids.length; ++i) {
		boids[i].tick();
	}

	grid.draw();
	for (var i = 0; i < boids.length; ++i) {
		boids[i].draw();
	}
	numTicks++;
}, 30);

canvas.addEventListener('mousedown', function(event) {
	var x = event.pageX - canvas.offsetLeft;
	var y = event.pageY - canvas.offsetTop;
	var col = Math.floor(x/scale);
	var row = Math.floor(y/scale);

	if (event.button == 0) {
		for (var i = col; i < col+2; ++i) {
			for (var j = row; j < row+2; ++j) {
				grid.cells[i][j].toggleObstacle();
			}
		}
	} else {
		grid.cells[col][row].toggleEnd();
	}
	grid.revalidate();
}, false);

canvas.addEventListener('contextmenu', function(event) { event.preventDefault(); }, false);
//
//------------------------- UTILS --------------------------------------------------
//
var edges = [
		{ x: 1, y: 0, w: 1 },
		{ x: 0, y: 1, w: 1 },
		{ x: -1, y: 0, w: 1 },
		{ x: 0, y: -1, w: 1 },
		{ x: 1, y: 1, w: 1.414213562 },
		{ x: 1, y: -1, w: 1.414213562 },
		{ x: -1, y: 1, w: 1.414213562 },
		{ x: -1, y: -1, w: 1.414213562 }
	];

var grayColors = [];
for (var i = 0; i <= 255; ++i) grayColors[i] = 'rgb('+i+','+i+','+i+')';
var blueColors = [];
for (var i = 0; i <= 255; ++i)  blueColors[i] = 'rgb(0,0,'+i+')';

function v2Normalize(v) {
	var l = v2Len(v);
	if (l != 0) {
		v[0] /= l;
		v[1] /= l;
	}
	return v;
}

function v2Inc(v, w) {
	v[0] += w[0];
	v[1] += w[1];
}

function v2Mul(v, s) {
	v[0] *= s;
	v[1] *= s;
	return v;
}

function v2Diff(v, w) {
	return [ v[0] - w[0], v[1] - w[1] ];
}

function v2Len(v) {
	return Math.sqrt(v[0]*v[0]+v[1]*v[1]);
}
